import { expect } from '../test_helper';
import pinYinReducer from '../../src/reducers/pinyin';
import { LIST } from '../../src/actions/types';

import { Combinations as Data } from '../../src/data/data';

describe('PinYin Reducer', () => {

    it('handles action with unknown type', () => {
        expect(pinYinReducer(undefined, {})).to.eql({ list: {} });
    });

    it('handles action of type LIST', () => {
        const action = { type: LIST, payload: Data };
        expect(pinYinReducer({ list: {} }, action)).to.eql({ list: Data });
    });

});