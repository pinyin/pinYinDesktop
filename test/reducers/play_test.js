import { expect } from '../test_helper';
import playReducer from '../../src/reducers/play';
import { PLAY_AUDIO, STOP_AUDIO } from '../../src/actions/types';

describe('Play Reducer', () => {

    it('handles action with unknown type', () => {
        expect(playReducer(undefined, {})).to.eql({ play: null });
    });

    it('handles action of type PLAY_AUDIO', () => {

        const payload = {
            label: "bā",
            nr: "1",
            tone: "ba1",
            state: "true"
        };

        const action = { type: PLAY_AUDIO, payload: payload };
        expect(playReducer(undefined, action)).to.eql({ play: payload });

    });

    it('handles action of type STOP_AUDIO', () => {
        const action = { type: STOP_AUDIO };
        expect(playReducer(undefined, action)).to.eql({ play: { play: null } });
    });

});