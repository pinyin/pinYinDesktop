import { renderComponent , expect } from '../test_helper';
import SoundButton from '../../src/components/SoundButton';

describe('SoundButton' , () => {
    let component;

    describe('Clicked' , () => {

        beforeEach(() => {

            const props = {
                tone: {
                    label: "bā",
                    nr: "1",
                    tone: "ba1",
                    state: "true"
                }
            };

            const state = {
                list: "",
                play: {
                    play: {
                        label: "bā",
                        nr: "1",
                        tone: "ba1",
                        state: "true"
                    }
                }
            };

            component = renderComponent(SoundButton, props, state);

        });

        it('renders something', () => {
            expect(component).to.exist;
        });

        it('class is active', () => {
            expect(component).to.have.class('active');
        });

    });

    describe('Inactive' , () => {

        beforeEach(() => {

            const props = {
                tone: {
                    label: "xiun",
                    nr: "4",
                    tone: "xun4"
                }
            };

            component = renderComponent(SoundButton, props);

        });

        it('renders something', () => {
            expect(component).to.exist;
        });

        it('class is not active', () => {
            expect(component).to.have.not.class('active');
        });

    });

});
