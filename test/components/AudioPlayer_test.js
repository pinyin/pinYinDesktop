import { renderComponent , expect } from '../test_helper';
import AudioPlayer from '../../src/components/AudioPlayer';

describe('AudioPlayer' , () => {
    let component;

    describe('Playing' , () => {

        beforeEach(() => {
            const props = {

                audio: {
                    play: {
                        label: "bā",
                        nr: 1,
                        state: true
                    }
                }

            };
            component = renderComponent(AudioPlayer, props);
        });

        it('renders something', () => {
            expect(component).to.exist;
        });

        it('has the correct label', () => {
            expect(component.find('label')).to.have.contain("bā");
        });

    });

    describe('Inactive' , () => {

        beforeEach(() => {
            const props = {

                audio: {
                    play: {
                        null
                    }
                }

            };
            component = renderComponent(AudioPlayer, props);
        });

        it('does not render', () => {
            expect(component).to.not.exist;
        });

    });

});
