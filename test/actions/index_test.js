import { expect } from '../test_helper';
import { PLAY_AUDIO, STOP_AUDIO, LIST } from '../../src/actions/types';
import { playAudio, stopAudio, list } from '../../src/actions';

import { Combinations as List } from '../../src/data/data';

describe('actions', () => {
    describe('playAudio', () => {
        it('has the correct type', () => {
            const action = playAudio();
            expect(action.type).to.equal(PLAY_AUDIO);
        });

        it('has the correct payload', () => {

            const payload = {
                label: "bā",
                nr: "1",
                tone: "ba1",
                state: "true"
            };

            const action = playAudio(payload);

            expect(action.payload).to.equal(payload);

        });
    });

    describe('stopAudio', () => {
        it('has the correct type', () => {
            const action = stopAudio();
            expect(action.type).to.equal(STOP_AUDIO);
        });

        it('has the correct payload', () => {
            const action = stopAudio();

            expect(action.payload).to.equal();
        });
    });

    describe('list', () => {
        it('has the correct type', () => {
            const action = list();
            expect(action.type).to.equal(LIST);
        });

        it('has the correct payload', () => {
            const action = list(List);
            expect(action.payload).to.equal(List);
        });
    });
});