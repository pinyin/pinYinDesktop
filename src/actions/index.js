import { PLAY_AUDIO, STOP_AUDIO, RESET_AUDIO, LIST } from './types';

export function playAudio(audio) {
    return {
        type: PLAY_AUDIO,
        payload: audio
    };
}

export function stopAudio() {
    return {
        type: STOP_AUDIO
    };
}

export function list(list) {
    return {
        type: LIST,
        payload: list
    };
}


