import { PLAY_AUDIO, STOP_AUDIO, RESET_AUDIO } from '../actions/types';

const INITIAL_STATE = { play: null };

export default function (state = INITIAL_STATE, action) {
    switch(action.type) {
        case PLAY_AUDIO:
            return { ...state, play: action.payload }
        case RESET_AUDIO:
            return { ...state, play: action.payload }
        case STOP_AUDIO:
            return { ...state, play: INITIAL_STATE }
        default:
            return state;
    }
}