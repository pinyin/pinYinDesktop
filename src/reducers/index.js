import { combineReducers } from 'redux';
import Play from './play';
import PinYinData from './pinyin';

const rootReducer = combineReducers({
  list: PinYinData,
  play: Play
});

export default rootReducer;