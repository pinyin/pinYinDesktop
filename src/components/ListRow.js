import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router';

import Waypoint from 'react-waypoint';
import SoundButton from './SoundButton';

import '../../style/ListRow.scss';

export default class ListRow extends Component {

    constructor(props) {

        super(props);

        this._handleWaypointEnter = this._handleWaypointEnter.bind(this);
        this._handleWaypointLeave = this._handleWaypointLeave.bind(this);

        this.state = {
            visible: "leave"
        };

    }

    _handleWaypointEnter() {
        this.setState({
            visible: "enter"
        });
    }

    _handleWaypointLeave() {
        this.setState({
            visible: "leave"
        });
    }

    render() {
        let row = this.props.row;
        let tone = this.props.row.tones;


        let viewClass = "ListRow";
        if (this.state.visible) viewClass += ' ' + this.state.visible;

        return (
            <div className={viewClass} key={row.combination}>
                <SoundButton key={tone[0].tone} tone={tone[0]} />
                <SoundButton key={tone[1].tone} tone={tone[1]} />
                <SoundButton key={tone[2].tone} tone={tone[2]} />
                <SoundButton key={tone[3].tone} tone={tone[3]} />
            </div>
        );
    }

}