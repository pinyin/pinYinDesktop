import React from 'react';
import { connect } from 'react-redux';
import { Component } from 'react';
import { bindActionCreators } from 'redux';
import { playAudio } from '../actions/index';
import Waypoint from 'react-waypoint';

import '../../style/SoundButton.scss';

export default class SoundButton extends Component {

    constructor(props) {

        super(props);

        this.playSound = this.playSound.bind(this);

    }

    shouldComponentUpdate(nextProps, nextState) {

        let tone = this.props.tone;
        let audio = nextProps.audio.play;

        if(this.active(audio, tone)) { // Set button to active
            tone.state = "true";
            return true;
        } else if(tone.state == "true") { // Set old buttons to false
            tone.state = "false";
            return true;
        } else { // Do not update other buttons
            return false;
        }

    }

    active(audio, tone) {
        if(audio.tone == tone.tone) return true;
    }

    playSound () {

        this.props.tone.state = "true";

        this.props.playAudio(this.props.tone);

    }

    render() {
        // Audio
        let tone = this.props.tone;
        let audio = this.props.audio.play;

        let viewClass = "SoundButton";

        if(audio && this.active(audio, tone)) viewClass += ' active';

        return (
            <div className={viewClass} onClick={this.playSound}>
                {tone.label}
            </div>
        );
    }

}

function mapStateToProps({play}) {

    return {
        audio: play
    };

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ playAudio }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SoundButton);