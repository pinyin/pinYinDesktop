import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Menu from './Menu';

import '../../style/App.scss';

export default class App extends Component {

    render() {
        return (
            <div className="App">
                <Menu></Menu>
                {this.props.children}
            </div>
        );
    }

}