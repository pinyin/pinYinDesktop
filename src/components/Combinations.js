import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { list, stopAudio } from '../actions/index';
import { Combinations as Data } from '../data/data';

import AudioController from './AudioController';
import ListSwitch from './ListSwitch';

class Finals extends Component {

    componentWillMount() {
        this.props.stopAudio();
        this.props.list(Data);
    }

    render() {

        return (
            <div className="Combinations">
                <ListSwitch title="Chart" next="finals" />
                <AudioController />
            </div>
        )

    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ list, stopAudio }, dispatch);
}

export default connect(null, mapDispatchToProps)(Finals);