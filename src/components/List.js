import React, { Component } from 'react';

import SoundButton from './SoundButton';
import ListRow from './ListRow';
import ListIndicator from './ListIndicator';

import '../../style/List.scss';

export default class List extends Component {

    renderList() {
        return this.props.list.map((group) => {

            return (
                <div className="group" data-group={group[0].initial} key={group[0].initial}>
                    {this.renderRow(group)}
                </div>
            );

        });

    }

    renderRow(group) {
        return group.map((row) => {

            return (
                <ListRow row={row} key={row.combination} />
            );

        });
    }

    render() {
        return (
            <div className="List">
                <ListIndicator />
                <div className="menu-spacer"></div>
                <div className="scroll">
                    {this.renderList()}
                </div>
            </div>
        );
    }

}


export default List;