import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router';

import '../../style/Menu.scss';

export default class Menu extends Component {
    render() {
        return (
            <div className="Menu">
                <Link to="chart" className="menuButton">Chart</Link>
                <Link to="initials" className="menuButton">Initials</Link>
                <Link to="finals" className="menuButton">Finals</Link>
                <Link to="what-is-pinyin" className="menuButton">Tones</Link>
            </div>
        );
    }
}