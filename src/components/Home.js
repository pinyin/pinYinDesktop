import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import Menu from './Menu';
import { Home as Data } from '../data/data';
import ListSwitch from './ListSwitch';

import '../../style/Home.scss';

export default class Home extends Component {

    render() {
        return (
            <div className="Home">
                <ListSwitch title="Home" next="chart" />

                <div className="center">

                    <div className="letters">
                        <div className="pin">
                            <div className="letter p">

                                <div className="dotsAndLine left purple">
                                    <div className="inner">
                                        <Link to="initials" className="menuButton hover">initial</Link>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>

                            </div>
                            <div className="letter i">

                                <div className="rooftop">

                                    <div className="dotsAndLine purple">
                                        <div className="inner">
                                            <Link to="what-is-pinyin" className="menuButton hover">tone</Link>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>

                                </div>

                                <div className="footerDots red">
                                    <Link to="finals" className="menuButton hover">final</Link>
                                    <span></span><span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span>
                                    <span></span>
                                </div>

                            </div>
                            <div className="letter n">

                                <div className="footerDots red">
                                    <span></span>
                                </div>

                            </div>
                        </div>
                        <div className="yin">
                            <div className="letter y">

                                <div className="dotsAndLine">
                                    <div className="inner">
                                        <Link to="chart" className="menuButton">initial</Link>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>

                            </div>
                            <div className="letter i">

                                <div className="rooftop">

                                    <div className="dotsAndLine">
                                        <div className="inner">
                                            <Link to="what-is-pinyin" className="menuButton">tone</Link>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </div>

                                </div>

                                <div className="footerDots">
                                    <Link to="finals" className="menuButton">final</Link>
                                    <span></span><span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span><span></span><span></span><span></span>
                                    <span></span><span></span><span></span>
                                    <span></span>
                                </div>

                            </div>
                            <div className="letter n">

                                <div className="footerDots">
                                    <span></span>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div className="description">
                    <div className="center">
                        { Data.info }
                    </div>
                </div>

            </div>
        );
    }
}