import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router';

import '../../style/About.scss';

export default class Menu extends Component {
    render() {
        return (
            <div className="About">
                <div className="innerRectangle">
                    <div className="corner">
                        <span></span>
                        <span></span>
                    </div>

                    <div className="header">
                        <div className="description">
                            Tones are used to distinguish the meaings of characters.
                        </div>
                        <div className="description">
                            Tone marks are written above <span>main vowel</span> of a syllable.
                        </div>
                    </div>

                    <div className="main">
                        <div className="tone-1"></div>
                        <div className="tone-2"></div>
                        <div className="tone-3"></div>
                        <div className="tone-4"></div>
                        <div className="tone-5"></div>
                    </div>

                    <div className="footer">
                        <div className="description">
                            The main vowel is determined by the order of <span>a o e i u v</span>
                        </div>
                        <div className="description">
                            Tone marks for <span>i & u</span> combination : <br/>always placed on the second vowel.
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}