import React, { Component } from 'react';

import '../../style/ListIndicator.scss';

export default class ListIndicator extends Component {

    constructor(props) {

        super(props);

        this.onScroll = this.onScroll.bind(this);
        this.requestTick = this.requestTick.bind(this);
        this.update = this.update.bind(this);

        this.state = {
            activeGroup: "",
            showIndicator: "hide"
        };

        this.lastScrollY = 0;
        this.ticking = false;
        this.groups = false;
        this.prevScrollY = 0;

    }

    componentDidMount() {
        this.lastScrollY = 0;
        window.addEventListener('scroll', this.onScroll.bind(this), false);
        this.groups = document.querySelectorAll('.group');    
    }

    onScroll() {
        this.lastScrollY = window.scrollY;
        this.requestTick();
    }

    requestTick() {
        if(!this.ticking) { 
            requestAnimationFrame(this.update);
            this.ticking = true;
        }
    }

    update() {

        var groups              = this.groups,
            lastScrollY         = this.lastScrollY,
            showTime            = 1500,
            showIndicatorTimeout = null,
            activeGroup         = false,
            group               = null,
            groupTop            = [],
            halfWindowHeight    = window.innerHeight * 0.5,
            offset              = 0;

        // Speed
        var speed               = Math.abs(this.lastScrollY - this.prevScrollY);    
        this.prevScrollY    = this.lastScrollY;

        if(speed > 30) {
            clearTimeout(showIndicatorTimeout);

            if(this.state.showIndicator == "hide") {
                this.setState({
                    showIndicator: "show"
                });
            }

            showIndicatorTimeout = setTimeout(this.showIndicator.bind(this), showTime);
        }

        // first loop is going to do all
        // the reflows (since we use offsetTop)
        for(var m = 0; m < groups.length; m++) {
            group       = groups[m];
            groupTop[m] = group.offsetTop;
        }

        // second loop is going to go through
        // the groups and add the left class
        // to the elements' classlist
        for(var m = 0; m < groups.length; m++) {

            group       = groups[m];

            if(lastScrollY > groupTop[m] - halfWindowHeight) {
                group.classList.add('left');
                activeGroup = group;
            }

        }

        if(activeGroup && activeGroup.dataset.group != this.state.activeGroup) {
            this.setState({
                activeGroup: activeGroup.dataset.group
            });
        }

        // this.setState({
        //         showIndicator: "show"
        //     });
        // showIndicatorTimeout = setTimeout(this.showIndicator.bind(this), showTime);

        // allow further rAFs to be called
        this.ticking = false;
    }

    showIndicator() {
        this.setState({
            showIndicator: "hide"
        });
    }

    render() {
        return (
            <div className="ListIndicator" data-show={this.state.showIndicator}>{this.state.activeGroup}</div>
        );
    }

}


export default ListIndicator;