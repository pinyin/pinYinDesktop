import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import List from './List';
import Waypoint from 'react-waypoint';
import AudioPlayer from './AudioPlayer';

import '../../style/AudioController.scss';

class AudioController extends Component {

    render() {

        // Audio
        let audio = this.props.audio;

        let viewClass = "AudioController";
        if (audio.play.state) viewClass += ' playing';

        // List
        let list = this.props.list;

        return (
            <div className={viewClass}>

                <div className="whatIsPinYin">
                    <div className="corner"></div>
                    <Link to="/">What is PinYin?</Link>
                    <div className="swirl"></div>
                </div>

                <div className="info">
                    <AudioPlayer audio={audio} />

                    <div className="listInfo">
                        <h3>{list.content.title}</h3>
                        <p>{list.content.info}</p>
                    </div>
                </div>

                <List list={list.list} />

            </div>
        )
    }
}

function mapStateToProps({list, play}) {

    return {
        list: list.list,
        audio: play
    };

}

export default connect(mapStateToProps, null)(AudioController);