import React from 'react';
import { Component } from 'react';
import { Link } from 'react-router';

import '../../style/ListSwitch.scss';

export default class ListSwitch extends Component {


    render() {
        return (
            <div className="ListSwitch">
                <label>{ this.props.title }</label>
                <Link to={ this.props.next } className="switch">
                    <span></span>
                    <span></span>
                    <span></span>
                </Link>
            </div>
        );
    }
}