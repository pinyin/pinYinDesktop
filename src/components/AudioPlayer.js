import React, { Component } from 'react';
import { Link } from 'react-router';

import '../../style/AudioPlayer.scss';

export default class AudioPlayer extends Component {

    constructor(props) {

        super(props);

        this.playSound = this.playSound.bind(this);
        this.playComplete = this.playComplete.bind(this);

    }

    soundID () {
        return this.props.audio.play.tone;
    }

    startSound () {

        let soundID = this.soundID();

        if(createjs.Sound.loadComplete(soundID)) {
            this.playSound();
        } else {
            createjs.Sound.on("fileload", this.playSound, this, true);
            let file = `assets/audio/${soundID}.mp3`;
            createjs.Sound.registerSound(file, soundID);
        }

    }

    playSound(event) {

        let soundID = this.soundID();

        var instance = createjs.Sound.play(soundID);
        instance.on("complete", this.playComplete, this);

    }

    playComplete(event) {
        // Sound has been played
    }

    render() {

        let audio = this.props.audio;

        if(!audio.play.state) return false;

        if("tone" in audio.play) this.startSound();

        return (
            <div className="AudioPlayer">
                <div className="audioInfo">
                    <label>{audio.play.label}</label>
                </div>
            </div>
        );


    }

}

export default AudioPlayer;