import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

import List from './List';
import Menu from './Menu';

import '../../style/Combinations.scss';

class Combinations extends Component {
    render() {
        const { menu, list } = this.props.combinations;

        return (
            <div className="Combinations list-view">
                <h3>Combinations</h3>

                <Menu menu={menu} />

                <List list={list} />
            </div>
        )
    }
}

function mapStateToProps({pinYinData}) {
    return {
        combinations: pinYinData.combinations
    };
}

//function mapDispatchToProps(dispatch) {
//    return bindActionCreators({ selectBook: selectBook }, dispatch);
//}

export default connect(mapStateToProps, null)(Combinations);