import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

import List from './List';
import Menu from './Menu';

import '../../style/Finals.scss';

class Finals extends Component {
    render() {
        const { menu, list } = this.props.finals;

        return (
            <div className="Finals list-view">
                <h3>Finals</h3>

                <Menu menu={menu} />

                <List list={list} />
            </div>
        )
    }
}

function mapStateToProps({pinYinData}) {
    return {
        finals: pinYinData.finals
    };
}

//function mapDispatchToProps(dispatch) {
//    return bindActionCreators({ selectBook: selectBook }, dispatch);
//}

export default connect(mapStateToProps, null)(Finals);