import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import { stopAudio } from '../actions/index';


import List from './List';
import Menu from './Menu';
import AudioPlayer from './AudioPlayer';

import '../../style/ListView.scss';

class Combinations extends Component {


    componentWillUpdate(nextProps, nextState) {
        console.log(nextProps);
        console.log(nextState);
        console.log('will update');
        //this.props.stopAudio();
    }

    render() {


        console.log(this.props.audio);

        console.log('render');
        let slug = "Combination";

        return (
            <div className={`ListView ${slug}`}>

                <div className="whatIsPinYin">
                    <div className="corner"></div>
                    <Link to="explenation">What is PinYin?</Link>
                    <div className="swirl"></div>
                </div>

                <AudioPlayer audio={this.props.audio.play} />

                <List list={this.props.route.path} />

            </div>
        )
    }
}

function mapStateToProps({pinYinData, play}) {
    return {
        data: pinYinData,
        audio: play
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ stopAudio }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Combinations);