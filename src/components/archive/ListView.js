import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import { list, resetAudio } from '../actions/index';
import PinYinData from '../data/data';

import List from './List';
import AudioPlayer from './AudioPlayer';

import '../../style/ListView.scss';

class ListView extends Component {

    componentWillMount() {

        console.log('mount LISTVIEW');

    }

    componentWillReceiveProps(nextProps) {

        console.log('receiving props LISTVIEW', nextProps);

        let content, list;

        console.log(this.props.route.path);

        switch(this.props.route.path) {
            case "finals":
                content = PinYinData.finals.content;
                list = PinYinData.finals.list;
                //data.slug = "Finals";
                break;
            case "initials":
                content = PinYinData.initials.content;
                list = PinYinData.initials.list;
                //data.slug = "Initials";
                break;
            default:
                content = PinYinData.combinations.content;
                list = PinYinData.combinations.list;
            //data.slug = "Combinations";
        }

        this.props.resetAudio(content);
        this.props.list(list);

    }

    render() {

        console.log('render LISTVIEW');

        return (
            <div className='ListView Finals'>

                <div className="whatIsPinYin">
                    <div className="corner"></div>
                    <Link to="explenation">What is PinYin?</Link>
                    <div className="swirl"></div>
                </div>

                <AudioPlayer />

                <List />

            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ list, resetAudio }, dispatch);
}

export default connect(null, mapDispatchToProps)(ListView);