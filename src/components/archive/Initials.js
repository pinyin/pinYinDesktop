import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';
import List from './List';
import Menu from './Menu';

import '../../style/Initials.scss';

class Initials extends Component {
    render() {
        const { menu, list } = this.props.initials;

        return (
            <div className="Initials list-view">
                <h3>Initials</h3>

                <Menu menu={menu} />

                <List list={list} />
            </div>
        )
    }
}

function mapStateToProps({pinYinData}) {

    return {
        initials: pinYinData.initials
    };
}

//function mapDispatchToProps(dispatch) {
//    return bindActionCreators({ selectBook: selectBook }, dispatch);
//}

export default connect(mapStateToProps, null)(Initials);