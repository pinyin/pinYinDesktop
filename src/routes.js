import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import Home from './components/Home';
import Combinations from './components/Combinations';
import Finals from './components/Finals';
import Initials from './components/Initials';
import About from './components/About';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="chart" component={Combinations} />
        <Route path="initials" component={Initials} />
        <Route path="finals" component={Finals} />
        <Route path="what-is-pinyin" component={About} />
    </Route>
)