import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, browserHistory } from 'react-router';

import App from './components/App';

import reducers from './reducers';
import routes from './routes';

import ga from 'react-ga';
ga.initialize('UA-77138438-1');

function logPageView() {
    ga.pageview(window.location.pathname);
}

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
      <Router history={ browserHistory } routes={routes} onUpdate={logPageView} />
  </Provider>
  , document.querySelector('.container'));
