import CombinationsGenerated from './combinations';

// TODO: tone should be with number ex. 'a1', instead of 'a'.

var PinYinData = {
    "menu": [
        {"url": "/", label: "Home"},
        {"url": "chart", label: "Chart"},
        {"url": "initials", label: "Initials"},
        {"url": "finals", label: "Finals"}
    ],
    "home": {
        "title": "Pin Yin",
        "info": "Hanyu Pinyin or Pinyin is the official phonetic system for transcribing the Mandarin pronunciations of Chinese characters into the Latin alphabet in mainland China, Taiwan and Singapore. It can be used to teach and learn Chinese, or as an input method for typing Chinese characters." 
    },
    "combinations": {
        "content": {
            "title": "PinYin chart",
            "info": "This chart consists of all possible combinations of initials and finals. Any sounds of characters can be found here. Have fun practicing!"
        },
        list: CombinationsGenerated.combinations,
        listBlob: CombinationsGenerated.blob
    },
    "initials": {
        "content": {
            "title": "Initials",
            "info": "The initials are the consonants, which are arranged in groups according to the way they are pronounced. It has its own pronunciation preference when it stands alone, and they are always pronounced with first tone. “b, p, m, f” with “o” sound; “d, t, n, l, g, k, h” with “e” sound; “j, q, x, z, c, s, zh, ch, sh, r” with “i” sound; “y” with “i” sound and “w” with “u” sound."
        },
        list: [
            [
                {
                    "combination": "bpmf",
                    "final": "o",
                    "tones": [
                        {
                            "tone": "b",
                            "label": "b"
                        },
                        {
                            "tone": "p",
                            "label": "p"
                        },
                        {
                            "tone": "m",
                            "label": "m"
                        },
                        {
                            "tone": "f",
                            "label": "f"
                        },
                    ]
                },
                {
                    "combination": "dtnl",
                    "final": "e",
                    "tones": [
                        {
                            "tone": "d",
                            "label": "d"
                        },
                        {
                            "tone": "t",
                            "label": "t"
                        },
                        {
                            "tone": "n",
                            "label": "n"
                        },
                        {
                            "tone": "l",
                            "label": "l"
                        },
                    ]
                },
                {
                    "combination": "gkh",
                    "final": "e",
                    "tones": [
                        {
                            "tone": "g",
                            "label": "g"
                        },
                        {
                            "tone": "k",
                            "label": "k"
                        },
                        {
                            "tone": "h",
                            "label": "h"
                        },
                        {
                            "tone": "h",
                            "label": "h"
                        },
                    ]
                },
                {
                    "combination": "jqx",
                    "final": "i",
                    "tones": [
                        {
                            "tone": "j",
                            "label": "j"
                        },
                        {
                            "tone": "q",
                            "label": "q"
                        },
                        {
                            "tone": "x",
                            "label": "x"
                        },
                        {
                            "tone": "x",
                            "label": "x"
                        },
                        ]
                    },
                    {
                    "combination": "zcs",
                    "final": "i",
                    "tones": [
                        {
                            "tone": "z",
                            "label": "z"
                        },
                        {
                            "tone": "c",
                            "label": "c"
                        },
                        {
                            "tone": "s",
                            "label": "s"
                        },
                        {
                            "tone": "s",
                            "label": "s"
                        },
                        ]
                    },
                    {
                    "combination": "zhchshr",
                    "final": "i",
                    "tones": [
                        {
                            "tone": "zh",
                            "label": "zh"
                        },
                        {
                            "tone": "ch",
                            "label": "ch"
                        },
                        {
                            "tone": "sh",
                            "label": "sh"
                        },
                        {
                            "tone": "r",
                            "label": "r"
                        },
                    ]
                },
                {
                    "combination": "yw",
                    "tones": [
                        {
                            "tone": "y",
                            "label": "y"
                        },
                        {
                            "tone": "w",
                            "label": "w"
                        },
                        {
                            "tone": "y",
                            "label": "y"
                        },
                        {
                            "tone": "w",
                            "label": "w"
                        },
                    ]
                },
             ]
         ]
    },

    "finals": {
        "content": {
            "title": "Finals",
            "info": "Finals are mostly vowels with exceptions of letters “r, n, ng”. There are single vowels and compound vowels. The Compound vowels are combinations of 2 or 3 vowels."
        },
        list: [
            [
                {
                    "tones": [
                        {
                            "tone": "a1",
                            "label": "ā"
                        },
                        {
                            "tone": "a2",
                            "label": "á"
                        },
                        {
                            "tone": "a3",
                            "label": "ǎ"
                        },
                        {
                            "tone": "a4",
                            "label": "à"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "o1",
                            "label": "ō"
                        },
                        {
                            "tone": "o2",
                            "label": "ó"
                        },
                        {
                            "tone": "o3",
                            "label": "ǒ"
                        },
                        {
                            "tone": "o4",
                            "label": "ò"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "e1",
                            "label": "ē"
                        },
                        {
                            "tone": "e2",
                            "label": "é"
                        },
                        {
                            "tone": "e3",
                            "label": "ě"
                        },
                        {
                            "tone": "e4",
                            "label": "è"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "i1",
                            "label": "ī"
                        },
                        {
                            "tone": "i2",
                            "label": "í"
                        },
                        {
                            "tone": "i3",
                            "label": "ǐ"
                        },
                        {
                            "tone": "i4",
                            "label": "ì"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "u1",
                            "label": "ū"
                        },
                        {
                            "tone": "u2",
                            "label": "ú"
                        },
                        {
                            "tone": "u3",
                            "label": "ǔ"
                        },
                        {
                            "tone": "u4",
                            "label": "ù"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ü1",
                            "label": "ǖ"
                        },
                        {
                            "tone": "ü2",
                            "label": "ǘ"
                        },
                        {
                            "tone": "ü3",
                            "label": "ǚ"
                        },
                        {
                            "tone": "ü4",
                            "label": "ǜ"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ai1",
                            "label": "āi"
                        },
                        {
                            "tone": "ai2",
                            "label": "ái"
                        },
                        {
                            "tone": "ai3",
                            "label": "ǎi"
                        },
                        {
                            "tone": "ai4",
                            "label": "ài"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ao1",
                            "label": "āo"
                        },
                        {
                            "tone": "ao2",
                            "label": "áo"
                        },
                        {
                            "tone": "ao3",
                            "label": "ǎo"
                        },
                        {
                            "tone": "ao4",
                            "label": "ào"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ou1",
                            "label": "ōu"
                        },
                        {
                            "tone": "ou2",
                            "label": "óu"
                        },
                        {
                            "tone": "ou3",
                            "label": "ǒu"
                        },
                        {
                            "tone": "ou4",
                            "label": "òu"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ei1",
                            "label": "ēi"
                        },
                        {
                            "tone": "ei2",
                            "label": "éi"
                        },
                        {
                            "tone": "ei3",
                            "label": "ěi"
                        },
                        {
                            "tone": "ei4",
                            "label": "èi"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "er1",
                            "label": "ēr"
                        },
                        {
                            "tone": "er2",
                            "label": "ér"
                        },
                        {
                            "tone": "er3",
                            "label": "ěr"
                        },
                        {
                            "tone": "er4",
                            "label": "èr"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ia1",
                            "label": "iā"
                        },
                        {
                            "tone": "ia2",
                            "label": "iá"
                        },
                        {
                            "tone": "ia3",
                            "label": "iǎ"
                        },
                        {
                            "tone": "ia4",
                            "label": "ià"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ie1",
                            "label": "iē"
                        },
                        {
                            "tone": "ie2",
                            "label": "ié"
                        },
                        {
                            "tone": "ie3",
                            "label": "iě"
                        },
                        {
                            "tone": "ie4",
                            "label": "iè"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "iu1",
                            "label": "iū"
                        },
                        {
                            "tone": "iu2",
                            "label": "iú"
                        },
                        {
                            "tone": "iu3",
                            "label": "iǔ"
                        },
                        {
                            "tone": "iu4",
                            "label": "iù"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ua1",
                            "label": "uā"
                        },
                        {
                            "tone": "ua2",
                            "label": "uá"
                        },
                        {
                            "tone": "ua3",
                            "label": "uǎ"
                        },
                        {
                            "tone": "ua4",
                            "label": "uà"
                        },
                    ]
                },
                 {
                    "tones": [
                        {
                            "tone": "uo1",
                            "label": "uō"
                        },
                        {
                            "tone": "uo2",
                            "label": "uó"
                        },
                        {
                            "tone": "uo3",
                            "label": "uǒ"
                        },
                        {
                            "tone": "uo4",
                            "label": "uò"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ui1",
                            "label": "uī"
                        },
                        {
                            "tone": "ui2",
                            "label": "uí"
                        },
                        {
                            "tone": "ui3",
                            "label": "uǐ"
                        },
                        {
                            "tone": "ui4",
                            "label": "uì"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "üe1",
                            "label": "üē"
                        },
                        {
                            "tone": "üe2",
                            "label": "üé"
                        },
                        {
                            "tone": "üe3",
                            "label": "üě"
                        },
                        {
                            "tone": "üe4",
                            "label": "üè"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "iao1",
                            "label": "iāo"
                        },
                        {
                            "tone": "iao2",
                            "label": "iáo"
                        },
                        {
                            "tone": "iao3",
                            "label": "iǎo"
                        },
                        {
                            "tone": "iao4",
                            "label": "iào"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "uai1",
                            "label": "uāi"
                        },
                        {
                            "tone": "uai2",
                            "label": "uái"
                        },
                        {
                            "tone": "uai3",
                            "label": "uǎi"
                        },
                        {
                            "tone": "uai4",
                            "label": "uài"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "an1",
                            "label": "ān"
                        },
                        {
                            "tone": "an2",
                            "label": "án"
                        },
                        {
                            "tone": "an3",
                            "label": "ǎn"
                        },
                        {
                            "tone": "an4",
                            "label": "àn"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "en1",
                            "label": "ēn"
                        },
                        {
                            "tone": "en2",
                            "label": "én"
                        },
                        {
                            "tone": "en3",
                            "label": "ěn"
                        },
                        {
                            "tone": "en4",
                            "label": "èn"
                        },
                    ]
                },

                {
                    "tones": [
                        {
                            "tone": "in1",
                            "label": "īn"
                        },
                        {
                            "tone": "in2",
                            "label": "ín"
                        },
                        {
                            "tone": "in3",
                            "label": "ǐn"
                        },
                        {
                            "tone": "in4",
                            "label": "ìn"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "un1",
                            "label": "ūn"
                        },
                        {
                            "tone": "un2",
                            "label": "ún"
                        },
                        {
                            "tone": "un3",
                            "label": "ǔn"
                        },
                        {
                            "tone": "un4",
                            "label": "ùn"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ün1",
                            "label": "ǖn"
                        },
                        {
                            "tone": "ün2",
                            "label": "ǘn"
                        },
                        {
                            "tone": "ün3",
                            "label": "ǚn"
                        },
                        {
                            "tone": "ün4",
                            "label": "ǜn"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ian1",
                            "label": "iān"
                        },
                        {
                            "tone": "ian2",
                            "label": "ián"
                        },
                        {
                            "tone": "ian3",
                            "label": "iǎn"
                        },
                        {
                            "tone": "ian4",
                            "label": "iàn"
                        },
                    ]
                },
                 {
                    "tones": [
                        {
                            "tone": "uan1",
                            "label": "uān"
                        },
                        {
                            "tone": "uan2",
                            "label": "uán"
                        },
                        {
                            "tone": "uan3",
                            "label": "uǎn"
                        },
                        {
                            "tone": "uan4",
                            "label": "uàn"
                        },
                    ]
                },
                 {
                    "tones": [
                        {
                            "tone": "üan1",
                            "label": "üān"
                        },
                        {
                            "tone": "üan2",
                            "label": "üán"
                        },
                        {
                            "tone": "üan3",
                            "label": "üǎn"
                        },
                        {
                            "tone": "üan4",
                            "label": "üàn"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ang1",
                            "label": "āng"
                        },
                        {
                            "tone": "ang2",
                            "label": "áng"
                        },
                        {
                            "tone": "ang3",
                            "label": "ǎng"
                        },
                        {
                            "tone": "ang4",
                            "label": "àng"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ong1",
                            "label": "ōng"
                        },
                        {
                            "tone": "ong2",
                            "label": "óng"
                        },
                        {
                            "tone": "ong3",
                            "label": "ǒng"
                        },
                        {
                            "tone": "ong4",
                            "label": "òng"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "eng1",
                            "label": "ēng"
                        },
                        {
                            "tone": "eng2",
                            "label": "éng"
                        },
                        {
                            "tone": "eng3",
                            "label": "ěng"
                        },
                        {
                            "tone": "eng4",
                            "label": "èng"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "ing1",
                            "label": "īng"
                        },
                        {
                            "tone": "ing2",
                            "label": "íng"
                        },
                        {
                            "tone": "ing3",
                            "label": "ǐng"
                        },
                        {
                            "tone": "ing4",
                            "label": "ìng"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "iang1",
                            "label": "iāng"
                        },
                        {
                            "tone": "iang2",
                            "label": "iáng"
                        },
                        {
                            "tone": "iang3",
                            "label": "iǎng"
                        },
                        {
                            "tone": "iang4",
                            "label": "iàng"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "iong1",
                            "label": "iōng"
                        },
                        {
                            "tone": "iong2",
                            "label": "ióng"
                        },
                        {
                            "tone": "iong3",
                            "label": "iǒng"
                        },
                        {
                            "tone": "iong4",
                            "label": "iòng"
                        },
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "uang1",
                            "label": "uāng"
                        },
                        {
                            "tone": "uang2",
                            "label": "uáng"
                        },
                        {
                            "tone": "uang3",
                            "label": "uǎng"
                        },
                        {
                            "tone": "uang4",
                            "label": "uàng"
                        },
                    ]
                },
            ]
        ]
    }
};

export default PinYinData;

const Combinations = PinYinData.combinations;
const Initials = PinYinData.initials;
const Finals = PinYinData.finals;
const Home = PinYinData.home;

export { Combinations, Initials, Finals, Home };