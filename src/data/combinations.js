import PinYinJs from './helper';

// Create DATA - JS data object
var PinYin = {


    raw : {

        b: ['a', 'o', 'i', 'u', 'ai', 'ao', 'ei', 'iao', 'ian', 'ie', 'an',	'en', 'in', 'ang', 'eng', 'ing'],
        p: ['a', 'o', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'ie', 'iao', 'ian', 'an', 'en', 'in', 'ang', 'eng', 'ing'],
        m: ['a', 'o', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'ie', 'iu', 'iao', 'an', 'en', 'in', 'ian', 'ang', 'eng', 'ing'],
        f: ['a', 'o', 'u', 'ou', 'ei', 'an', 'en', 'ang', 'eng'],

        d: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'ie', 'iu', 'iao', 'uo', 'ui', 'an', 'en', 'un', 'ian', 'uan', 'ang', 'ong', 'eng', 'ing'],
        t: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'ie', 'iao', 'uo', 'ui',  'an', 'un', 'ian', 'uan', 'ang', 'eng', 'ing'],
        n: ['a', 'e', 'i', 'u', 'ü', 'ai', 'ao', 'ou', 'ei', 'ie', 'iu', 'iao', 'uo', 'üe', 'an', 'en', 'in', 'ian', 'uan', 'ang', 'ong', 'eng', 'ing', 'iang'],
        l: ['a', 'e', 'i', 'u', 'ü', 'ai', 'ao', 'ou', 'ei', 'ie', 'iu', 'iao', 'uo', 'üe', 'an', 'in', 'un', 'ian', 'uan', 'ang', 'ong', 'eng', 'ing', 'iang'],

        g: ['a', 'e', 'u', 'ai', 'ao', 'ou', 'ei', 'ua', 'uo', 'ui', 'uai', 'an', 'en', 'un', 'uan', 'ang', 'ong', 'eng', 'uang'],
        k: ['a', 'e', 'u', 'ai', 'ao', 'ou', 'ei', 'ua', 'uo', 'ui', 'uai', 'an', 'en', 'un', 'uan', 'ang', 'ong', 'eng', 'uang'],
        h: ['a', 'e', 'u', 'ai', 'ao', 'ou', 'ei', 'ua', 'uo', 'ui', 'uai', 'an', 'en', 'un', 'uan', 'ang', 'ong', 'eng', 'uang'],

        j: ['i', 'u', 'ia', 'ie', 'iu', 'ue', 'iao', 'in', 'un', 'ian', 'uan', 'ing', 'iang', 'iong'],
        q: ['i', 'u', 'ia', 'ie', 'iu', 'ue', 'iao', 'in', 'un', 'ian', 'uan', 'ing', 'iang', 'iong'],
        x: ['i', 'u', 'ia', 'ie', 'iu', 'ue', 'iao', 'in', 'un', 'ian', 'uan', 'ing', 'iang', 'iong'],

        z: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'uo', 'ui', 'an', 'en', 'un', 'uan', 'ang', 'eng', 'ong'],
        c: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'uo', 'ui', 'an', 'en', 'un', 'uan', 'ang', 'eng', 'ong'],
        s: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'uo', 'ui', 'an', 'en', 'un', 'uan', 'ang', 'eng', 'ong'],

        zh: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'ua', 'uo', 'ui', 'uai', 'an', 'en', 'un', 'uan', 'ang', 'ong', 'eng', 'uang'],
        ch: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'ua', 'uo', 'ui', 'uai', 'an', 'en', 'un', 'uan', 'ang', 'ong', 'eng', 'uang'],
        sh: ['a', 'e', 'i', 'u', 'ai', 'ao', 'ou', 'ei', 'ua', 'uo', 'ui', 'uai', 'an', 'en', 'un', 'uan', 'ang', 'eng', 'uang'],
        r: ['e', 'i', 'u', 'ao', 'ou', 'uo', 'ui', 'an', 'en', 'un', 'uan', 'ang', 'ong', 'eng'],

        y: ['a', 'e', 'i', 'u', 'ao', 'ou', 'ue', 'an', 'in', 'un', 'uan', 'ang', 'ong', 'ing'],
        w: ['a', 'o', 'u', 'ai', 'ei', 'an', 'en', 'ang', 'eng'],
       
    },

    init: function() {

        for(var initial in this.raw) {

            var PinYinItems = Array();

            for (var c = 0; c < this.raw[initial].length; c++) {

                var final = this.raw[initial][c],
                    combination = initial + final;

                // Item
                var PinYinItem = {
                    initial: initial,
                    final: final,
                    combination: combination,
                    tones: this.getTones(combination)
                };

                PinYinItems.push(PinYinItem);

            }

            this.combinations.push(PinYinItems);
        }

    },

    getTones: function(combination) {

        var tones = Array();

        for (var toneNr = 1; toneNr <= 4; toneNr++) {
            var label = PinYinJs.convert(combination+toneNr);
            tones.push({tone: combination + toneNr, label: label, nr: toneNr})
        }

        return tones;

    },

    combinations: []

};

PinYin.init();

// Create DATA - Blobs
var PinYinBlobs = {

    combinations: {
        data: [],
        sectionIDs: [],
        rowIDs: []
    },

    init: function() {

        // COMBINATIONS
        var combinations = PinYin.combinations,
            dataBlob = {},
            sectionIDs = [],
            rowIDs = [],
            combinations_group,
            combination,
            i,
            j;

        for (var i = 0; i < combinations.length; i++) {
            // SECTIONS
            combinations_group = combinations[i];

            sectionIDs.push(combinations_group[0].initial);
            dataBlob[combinations_group[0].initial] = combinations_group[0].initial;

            // ROWS

            // Initialize Empty RowID Array for Section Index
            rowIDs[i] = [];

            for(j = 0; j < combinations_group.length; j++) {
                combination = combinations_group[j];

                // Add Unique Row ID to RowID Array for Section
                rowIDs[i].push(combination.combination);

                // Set Value for unique Section+Row Identifier that will be retrieved by getRowData
                dataBlob[combinations_group[0].initial + ':' + combination.combination] = combination;
            }
        }

        this.combinations.data = dataBlob;
        this.combinations.sectionIDs = sectionIDs;
        this.combinations.rowIDs = rowIDs;

    }

};

PinYinBlobs.init();

export default { combinations: PinYin.combinations, blob: PinYinBlobs };