import { renderComponent , expect } from '../test_helper';
import AudioPlayer from '../../src/components/AudioPlayer';

describe('AudioPlayer' , () => {
    let component;

    beforeEach(() => {
        const props = {

            audio: {
                 play: {
                    label: "bā",
                    nr: 1,
                    state: true
                }
            }

        };
        component = renderComponent(AudioPlayer, props);
    });

    it('renders something', () => {
        expect(component).to.exist;
    });
});
