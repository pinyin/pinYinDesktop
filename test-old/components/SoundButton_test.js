import { renderComponent , expect } from '../test_helper';
import SoundButton from '../../src/components/SoundButton';

describe('SoundButton' , () => {
    let component;

    beforeEach(() => {

        const props2 = {
            tone: {
                label: "xiun",
                nr: "4",
                tone: "xun4"
            },
            audio: {
                play: {
                    play: "hoi"
                }
            }
        };

        const props = { list: ['New comment', 'Other comment'] };


        component = renderComponent(SoundButton, props2, props);

    });

    it('renders something', () => {
        expect(component).to.exist;
    });
});
